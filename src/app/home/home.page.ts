import { Component , NgZone,OnInit  } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit  {
  devices:any[] = [];
  constructor( private ble:BLE,private ngZone: NgZone) 
  {
 
  }



  ngOnInit() {
   /*  this.ble.isEnabled()
    .then(result=>{
   console.log('result'+ result);
    }).catch(err=>{
     console.log('err'+err);
     this.enableBluetooth()
    }) */
  }

  enableBluetooth()
  {
    this.ble.enable()
    .then(result=>{
      console.log('result'+ result);
      alert("is enabled !")
       }).catch(err=>{
        console.log('err'+err);
       })
  }

  Scan(){
    this.devices = [];
    this.ble.scan([],15).subscribe(
      device => this.onDeviceDiscovered(device)
    );
  }
  onDeviceDiscovered(device){
    console.log('Discovered' + JSON.stringify(device,null,2));
    this.ngZone.run(()=>{
      this.devices.push(device)
     // this.Connect(device.id)
      console.log(device)
    })
  }

  Connect(device)
  {
    alert("clicked")
  
      this.ble.connect(device.id).subscribe(
        success=>console.log('sss'+success),
        errr=> console.log('err'+JSON.stringify(errr))
        )
      
      
    
   // ble.connect(device_id, connectCallback, disconnectCallback);
  }

}
