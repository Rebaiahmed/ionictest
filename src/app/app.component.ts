import { Component,OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CodePush, InstallMode, SyncStatus } from '@ionic-native/code-push/ngx';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent  implements OnInit  {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private codePush: CodePush,
    private alertController : AlertController
  ) {
    this.initializeApp();
    this.checkCodePush();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    // ...
    this.checkCodePush();
  }


  restartApp()
  {
    this.codePush.restartApplication();
  }

  checkCodePush() {
    
    this.codePush.sync({
     updateDialog: {
      appendReleaseDescription: true,
      descriptionPrefix: "\n\nChange log:\n"   
     },
     installMode: InstallMode.IMMEDIATE
  }).subscribe(
    (data) => {
     console.log('CODE PUSH SUCCESSFUL: ' + data);
     this.displayMessage(data);
     //alert("update codepush"+data);
     switch (data) {

      case SyncStatus.UP_TO_DATE:
        this.displayMessage("The application is up to date.");
        break;
      case SyncStatus.UPDATE_IGNORED:
        this.displayMessage("The user decided not to install the optional update.");
        break;
      case SyncStatus.ERROR:
        this.displayMessage("An error occurred while checking for updates");
        break; 
    }
     
    },
    (err) => {
     console.log('CODE PUSH ERROR: ' + err);
     
    }
  );
 }

 

async displayMessage(message) {
  const alert = await this.alertController.create({
    header: '',
    subHeader: '',
    message: message,
    buttons: ['OK']
  });

  await alert.present();
}
}
